/// This is my rendition of the fizz buzz challenge in as few lines of code as I can think of
// c.2020 Scott Hasserd
// v. 1.0 The finished product
// it lists every number 1 to 100 but for multiples of 3 it lists fizz
// multiples of 5 it lists buzz and multiples of both it lists fizzbuzz

#include <iostream>

using namespace std;

int main() {

	int number = 1;
	string word;

	do { 

		if ( number % 3 == 0 && number % 5 == 0) {
		word = "FIZZBUZZ";	
		}
		
		else if ( number % 3 == 0 ) {
		word = "fizz";
		}

		else if ( number % 5 == 0 ) {
		word = "buzz";
		}

		if (number % 3 == 0 || number % 5 == 0) {cout << (word) << endl;}
			else { cout << number << endl;}

		number = number + 1;
	}
		while ( number <= 100 );
}
